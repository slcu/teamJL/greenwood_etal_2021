# Greenwood_etal_2021

Source data and project code for Greenwood et al., "A spatial model of the plant circadian clock reveals design principles for coordinated timing".

/source_data/... contains the source data for the main and expanded view figures. 

Run simulate_LDLL.m for model simulations under light-dark (LD) cycles transferred to constant light (LL). 

Run simulate_LDLD.m for model simulations maintained under LD cycles. 

All variations of the model simulations, i.e. coupling regime, coupling gene, coupling strength, noise, are described within the program comments. 

The output of the programs is a .MAT file for each simulation, containing the 3x3 regions of interest (trace_cotyl, trace_hypoc, ect.) and a 3D matrix containing the simulated gene expression values on the seedling template for each time point (all_idata). 
