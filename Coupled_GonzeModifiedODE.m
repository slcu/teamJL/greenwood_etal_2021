function xdot = Coupled_GonzeModifiedODE_v2(t, x, Timescaling, Lsens,...
    couplinggene, JJ_local, JJ_long, JJ_global, nn, neighbors, celltype, L)

cell = size(Lsens, 1);
dim = 12;
xdot = zeros(cell * dim, 1);

all_t = 0:0.1:480;
Light = zeros(cell, 1);
Dark = zeros(size(Light));

for j = 1:cell    
    [~, k] = nanmin(abs(all_t - t)); % get index of t in relation to the timeseries 
    LAmp = L(j, k);   
    Light(j) = LAmp; 
end
    
Dark(Light == 0) = 1; 

% paravec(1) = 3.0599;
% paravec(2) = 0.2676;
% paravec(3) = 0.3089;
% paravec(4) = 4.0770;

% % Newly optimised parameters (27/05/2021)
paravec(1) = 2.8045;
paravec(2) = 0.2766;
paravec(3) = 0.5663;
paravec(4) = 1.7321;

%1 CL_m, name = CCA1/LHY mRNA, affected by kineticLaw
%2 CL_p, name = CCA1/LHY protein, affected by kineticLaw
%3 P97_m, name = PRR9/PRR7 mRNA, affected by kineticLaw
%4 P97_p, name = PRR9/PRR7 protein, affected by kineticLaw
%5 P51_m, name = PRR5/TOC1 mRNA, affected by kineticLaw
%6 P51_p, name = PRR5/TOC1 protein, affected by kineticLaw
%7 EL_m, name = ELF4/LUX mRNA, affected by kineticLaw
%8 EL_p, name = ELF4/LUX protein, affected by kineticLaw
%9 PIF_m, name = PIF4/PIF5 mRNA, affected by kineticLaw
%10 PIF_p, name = PIF4/PIF5 protein, affected by kineticLaw
%11 hypocotyl, name = Hypocotyl length, affected by kineticLaw
%12 P, name = P, affected by kineticLaw

% Compartment: id = cell, name = cell, constant
    compartment_cell = 1.0;
% Parameter:   id =  v1, name = v1
    global_par_v1 = 4.58;
% Parameter:   id =  v1L, name = v1L
    global_par_v1L = 3.0;
% Parameter:   id =  v2A, name = v2A
    global_par_v2A = 1.27;
% Parameter:   id =  v2B, name = v2B
%   global_par_v2B=1.48;
% Parameter:   id =  v3, name = v3
    global_par_v3 = 1.0;
% Parameter:   id =  v4, name = v4
    global_par_v4 = 1.47;
% Parameter:   id =  k1L, name = k1L
    global_par_k1L = 0.53;
% Parameter:   id =  v2L, name = v2L
    global_par_v2L = 5.0;
% Parameter:   id =  v3L, name = v3L
%   global_par_v3L=5.0;
    global_par_v3L = 0.0;
% Parameter:   id =  k1D, name = k1D
    global_par_k1D = 0.21;
% Parameter:   id =  k2, name = k2
    global_par_k2 = 0.35;
% Parameter:   id =  k3, name = k3
    global_par_k3 = 0.56;
% Parameter:   id =  k4, name = k4
    global_par_k4 = 0.57;
% Parameter:   id =  p1, name = p1
    global_par_p1 = 0.76;
% Parameter:   id =  p1L, name = p1L
    global_par_p1L = 0.42;
% Parameter:   id =  p2, name = p2
    global_par_p2 = 1.01;
% Parameter:   id =  p3, name = p3
    global_par_p3 = 0.64;
% Parameter:   id =  p4, name = p4
    global_par_p4 = 1.01;
% Parameter:   id =  d1, name = d1
    global_par_d1 = 0.68;
% Parameter:   id =  d2D, name = d2D
    global_par_d2D = 0.5;
% Parameter:   id =  d2L, name = d2L
    global_par_d2L = 0.29;
% Parameter:   id =  d3D, name = d3D
    global_par_d3D = 0.48;
% Parameter:   id =  d3L, name = d3L
    global_par_d3L = 0.38;%0.78;
% Parameter:   id =  d4D, name = d4D
    global_par_d4D = 1.21;
% Parameter:   id =  d4L, name = d4L
    global_par_d4L = 0.38;
% Parameter:   id =  K1, name = K1
    global_par_K1 = 0.16;
% Parameter:   id =  K0, name = K0
%   global_par_K0=0.16;
    global_par_K0 = paravec(1);
% Parameter:   id =  K2, name = K2
    global_par_K2 = 1.18;
% Parameter:   id =  K3, name = K3
%   global_par_K3=0.24;
% Parameter:   id =  K4, name = K4
%   global_par_K4=0.23;
    global_par_K4 = paravec(2);
% Parameter:   id =  K5, name = K5
%   global_par_K5=0.3;
    global_par_K5 = paravec(3);
% Parameter:   id =  K5b, name = K5b
    global_par_K5b = paravec(4);
% Parameter:   id =  K6, name = K6
    global_par_K6 = 0.46;
% Parameter:   id =  K7, name = K7
    global_par_K7 = 2.0;
% Parameter:   id =  K8, name = K8
    global_par_K8 = 0.36;
% Parameter:   id =  K9, name = K9
    global_par_K9 = 1.9;
% Parameter:   id =  K10, name = K10
    global_par_K10 = 1.9;
% Parameter:   id =  v5, name = v5
    global_par_v5 = 0.1;
% Parameter:   id =  k5, name = k5
    global_par_k5 = 0.14;
% Parameter:   id =  p5, name = p5
    global_par_p5 = 0.62;
% Parameter:   id =  d5L, name = d5L
    global_par_d5L = 4.0;
% Parameter:   id =  d5D, name = d5D
    global_par_d5D = 0.52;
% Parameter:   id =  g1, name = g1
    global_par_g1 = 0.01;
% Parameter:   id =  g2, name = g2
    global_par_g2 = 0.12;
% Parameter:   id =  K11, name = K11
    global_par_K11 = 0.21;
% Parameter:   id =  K12, name = K12
    global_par_K12 = 0.56;
% Parameter:   id =  PP, name = Photoperiod
    global_par_PP = 12.0;

mean_roottip = 0.0;
norm_roottip = 0.0;
mean_hypoc = 0.0;
norm_hypoc = 0.0;
for j = 1:cell
  if celltype(j) == 4
    mean_roottip = mean_roottip + x((j-1) * dim + couplinggene);
    norm_roottip = norm_roottip + 1.0;
  end
  if celltype(j) == 2
    mean_hypoc = mean_hypoc + x((j - 1) * dim + couplinggene);
    norm_hypoc = norm_hypoc + 1.0;
  end
end
mean_roottip = mean_roottip / norm_roottip;
mean_hypoc = mean_hypoc / norm_hypoc;

for j = 1:cell

  % Reaction: id = CL_transcription
   %reaction_CL_transcription=global_par_v1/(1+(x((j-1)*dim+4)/global_par_K1)^2+(x((j-1)*dim+6)/global_par_K2)^2);
    reaction_CL_transcription = global_par_v1 / (1 + (x((j - 1) * dim + 2)...
        / global_par_K0) ^2 + (x((j - 1) * dim + 4) / global_par_K1) ^2 +...
        (x((j - 1) * dim + 6) / global_par_K2) ^ 2);

  % Reaction: id = CL_light_transcription
   %reaction_CL_light_transcription=global_par_v1L*Light*Lsens(j)*x((j-1)*dim+12)/(1+(x((j-1)*dim+4)/global_par_K1)^2+(x((j-1)*dim+6)/global_par_K2)^2);
    reaction_CL_light_transcription = global_par_v1L * Light(j) * Lsens(j)...
        * x((j - 1) * dim + 12) / (1 + (x((j - 1) * dim + 2) /...
        global_par_K0) ^2 + (x((j - 1) * dim + 4) / global_par_K1) ^2 +...
        (x((j - 1) * dim + 6) / global_par_K2) ^2);

  % Reaction: id = CLm_light_degradation
    reaction_CLm_light_degradation = global_par_k1L * Light(j) * Lsens(j)...
        * x((j - 1) * dim + 1);

  % Reaction: id = CLm_dark_degradation
    reaction_CLm_dark_degradation = global_par_k1D * Dark(j) * x((j - 1) *...
        dim + 1);

  % Reaction: id = CL_translation
    reaction_CL_translation = global_par_p1 * x((j - 1) * dim + 1);

  % Reaction: id = CL_light_translation
    reaction_CL_light_translation = global_par_p1L * Light(j) * Lsens(j) *...
        x((j - 1) * dim + 1);

  % Reaction: id = CLp_degradation
    reaction_CLp_degradation = global_par_d1 * x((j - 1) * dim + 2);

  % Reaction: id = P97_light_transcription
   %reaction_P97_light_transcription=global_par_v2L*Light*Lsens(j)*x((j-1)*dim+12)/(1+(x((j-1)*dim+6)/global_par_K4)^2+(x((j-1)*dim+8)/global_par_K5)^2);
    reaction_P97_light_transcription = global_par_v2L * Light(j) *...
        Lsens(j) * x((j - 1) * dim + 12) / (1 + (x((j - 1) * dim + 6) /...
        global_par_K4) ^2 +(x((j - 1) * dim + 8) / global_par_K5) ^2 + ...
        (x((j - 1) * dim + 2) / global_par_K5b) ^2);
 
  % Reaction: id = P97_transcription
   %reaction_P97_transcription=global_par_v2A/(1+(x((j-1)*dim+6)/global_par_K4)^2+(x((j-1)*dim+8)/global_par_K5)^2);
    reaction_P97_transcription = global_par_v2A / (1 + (x((j - 1) * dim + 6)...
        / global_par_K4) ^2 + (x((j - 1) * dim + 8) / global_par_K5) ^2 +...
        (x((j - 1) * dim + 2) / global_par_K5b) ^2);

  % Reaction: id = P97_CL_transcription
   %reaction_P97_CL_transcription=global_par_v2B*x((j-1)*dim+2)^2/(global_par_K3^2+x((j-1)*dim+2)^2)/(1+(x((j-1)*dim+6)/global_par_K4)^2+(x((j-1)*dim+8)/global_par_K5)^2);
    reaction_P97_CL_transcription = 0.0;

  % Reaction: id = P97m_degradation
    reaction_P97m_degradation = global_par_k2 * x((j - 1) * dim + 3);

  % Reaction: id = P97_translation
    reaction_P97_translation = global_par_p2 * x((j - 1) * dim + 3);

  % Reaction: id = P97_dark_degradation
    reaction_P97_dark_degradation = global_par_d2D * Dark(j) * x((j-1) * ...
        dim + 4);

  % Reaction: id = P97_light_degradation
    reaction_P97_light_degradation = global_par_d2L * Light(j)...
         * x((j - 1) * dim + 4);

  % Reaction: id = P51_transcription
   %reaction_P51_transcription=global_par_v3/(1+(x((j-1)*dim+2)/global_par_K6)^2+(x((j-1)*dim+6)/global_par_K7)^2);
    reaction_P51_transcription = (global_par_v3 + global_par_v3L * Light(j)...
         * Lsens(j) * x((j - 1) * dim + 12)) / (1 + (x((j - 1) * dim + 2)...
          /global_par_K6) ^2 +(x((j - 1) * dim + 6) / global_par_K7) ^2);

  % Reaction: id = P51m_degradation
    reaction_P51m_degradation = global_par_k3 * x((j - 1) * dim + 5);

  % Reaction: id = P51_translation
    reaction_P51_translation = global_par_p3 * x((j - 1) * dim + 5);

  % Reaction: id = P51_dark_degradation
    reaction_P51_dark_degradation = global_par_d3D * Dark(j) * x((j - 1)...
         * dim + 6);

  % Reaction: id = P51_light_degradation
    reaction_P51_light_degradation = global_par_d3L * Light(j)...
         * x((j - 1) * dim + 6);

  % Reaction: id = EL_light_transcription
    reaction_EL_light_transcription = Light(j) * Lsens(j) * global_par_v4 /...
         (1 + (x((j - 1) * dim + 2) / global_par_K8) ^2+(x((j - 1) * dim +...
         6) / global_par_K9) ^2 + (x((j - 1) * dim + 8) / global_par_K10) ^2);

  % Reaction: id = ELm_degradation
    reaction_ELm_degradation = global_par_k4 * x((j - 1) * dim + 7);

  % Reaction: id = EL_translation
    reaction_EL_translation = global_par_p4 * x((j - 1) * dim + 7);

  % Reaction: id = EL_dark_degradation
    reaction_EL_dark_degradation = global_par_d4D * Dark(j) * x((j - 1) * ...
        dim + 8);

  % Reaction: id = EL_light_degradation
    reaction_EL_light_degradation = global_par_d4L * Light(j) * Lsens(j) * ...
        x((j - 1) * dim + 8);

  % Reaction: id = P_dark_accumulation
    reaction_P_dark_accumulation = 0.3 * (1 - x((j - 1) * dim + 12)) * ...
        Dark(j);

  % Reaction: id = P_light_degradation
    reaction_P_light_degradation = x((j - 1) * dim + 12) * Light(j) * ...
        Lsens(j);

  % Reaction: id = PIF_transcription
    reaction_PIF_transcription = global_par_v5 / (1 + (x((j - 1) * dim + 8)...
         / global_par_K11) ^2);

  % Reaction: id = PIFm_degradation
    reaction_PIFm_degradation = global_par_k5 * x((j - 1) * dim + 9);

  % Reaction: id = PIF_translation
    reaction_PIF_translation = global_par_p5 * x((j - 1) * dim + 9);

  % Reaction: id = PIF_dark_degradation
    reaction_PIF_dark_degradation = global_par_d5D * Dark(j) * x((j - 1) *...
        dim + 10);

  % Reaction: id = PIF_light_degradation
    reaction_PIF_light_degradation = global_par_d5L * Light(j) * Lsens(j)...
         * x((j - 1) * dim + 10);

  % Reaction: id = basal_growth
    reaction_basal_growth = global_par_g1;

  % Reaction: id = PIF_induced_growth
    reaction_PIF_induced_growth = global_par_g2 * x((j - 1) * dim + 10)...
         ^2 / (global_par_K12 ^ 2 + x((j - 1) * dim + 10) ^ 2);

  % Species:   id = CL_m, name = CCA1/LHY mRNA, affected by kineticLaw
    xdot((j-1)*dim+1) = (1 / (compartment_cell)) *...
        (( 1.0 * reaction_CL_transcription)+...
        (1.0 * reaction_CL_light_transcription) +...
        (-1.0 * reaction_CLm_light_degradation) +...
        (-1.0 * reaction_CLm_dark_degradation));
        
  % Species:   id = CL_p, name = CCA1/LHY protein, affected by kineticLaw
    xdot((j - 1) * dim + 2) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_CL_translation) +...
        (1.0 * reaction_CL_light_translation) +...
        (-1.0 * reaction_CLp_degradation));
        
  % Species:   id = P97_m, name = PRR9/PRR7 mRNA, affected by kineticLaw
    xdot((j - 1) * dim + 3) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_P97_light_transcription) +...
        ( 1.0 * reaction_P97_transcription) +...
        ( 1.0 * reaction_P97_CL_transcription) +...
        (-1.0 * reaction_P97m_degradation));
        
  % Species:   id = P97_p, name = PRR9/PRR7 protein, affected by kineticLaw
    xdot((j - 1) * dim + 4) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_P97_translation) +...
        (-1.0 * reaction_P97_dark_degradation) +...
        (-1.0 * reaction_P97_light_degradation));
        
  % Species:   id = P51_m, name = PRR5/TOC1 mRNA, affected by kineticLaw
    xdot((j - 1) * dim + 5) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_P51_transcription) +...
        (-1.0 * reaction_P51m_degradation));
        
  % Species:   id = P51_p, name = PRR5/TOC1 protein, affected by kineticLaw
    xdot((j - 1) * dim + 6) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_P51_translation) +...
        (-1.0 * reaction_P51_dark_degradation) +...
        (-1.0 * reaction_P51_light_degradation));
        
  % Species:   id = EL_m, name = ELF4/LUX mRNA, affected by kineticLaw
    xdot((j - 1) * dim + 7) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_EL_light_transcription) +...
        (-1.0 * reaction_ELm_degradation));
        
  % Species:   id = EL_p, name = ELF4/LUX protein, affected by kineticLaw
    xdot((j - 1) * dim + 8) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_EL_translation) +...
        (-1.0 * reaction_EL_dark_degradation) +...
        (-1.0 * reaction_EL_light_degradation));
        
  % Species:   id = PIF_m, name = PIF4/PIF5 mRNA, affected by kineticLaw
    xdot((j - 1) * dim + 9) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_PIF_transcription) +...
        (-1.0 * reaction_PIFm_degradation));
        
  % Species:   id = PIF_p, name = PIF4/PIF5 protein, affected by kineticLaw
    xdot((j - 1) * dim + 10) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_PIF_translation) +...
        (-1.0 * reaction_PIF_dark_degradation) +...
        (-1.0 * reaction_PIF_light_degradation));
        
  % Species:   id = hypocotyl, name = Hypocotyl length, affected by kineticLaw
    xdot((j - 1) * dim + 11) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_basal_growth) + (1.0 * reaction_PIF_induced_growth));

  % Species:   id = P, name = P, affected by kineticLaw
    xdot((j - 1) * dim + 12) = (1 / (compartment_cell)) *...
        ((1.0 * reaction_P_dark_accumulation) +...
        (-1.0 * reaction_P_light_degradation));

  %%%%%% Timescaling each parameter 
  for i = 1:12
    xdot((j - 1) * dim + i) = (1.0 / Timescaling(j)) * xdot((j - 1) * dim + i); 
  end

  %%%%%% Local coupling 
  if nn == 0
  	mean_local = 0; norm_local = 1;
  else
    mean_local = 0; norm_local = 0;
      for i = 1:nn 
        if neighbors(j, i) ~= 0
          mean_local = mean_local + x((neighbors(j, i) - 1) * dim + couplinggene);
          norm_local = norm_local + 1.0;
        end
      end
  end
  
  mean_local = mean_local / norm_local;
  xdot((j - 1) * dim + couplinggene) = xdot((j - 1) * dim + couplinggene) + ...
      JJ_local * (mean_local - x((j - 1) * dim + couplinggene));

  %%%%%% Long-distance coupling
  if celltype(j) == 4
    xdot((j - 1) * dim + couplinggene) = xdot((j - 1) * dim + couplinggene) + ...
        JJ_long * (mean_hypoc - x((j - 1) * dim + couplinggene));
  end

end

%%%%%% Global coupling
meanfield = 0;
for j = 1:cell
    meanfield = meanfield + x((j - 1) * dim + couplinggene);
end

meanfield = meanfield / cell;

for j = 1:cell
    xdot((j - 1) * dim + couplinggene) =...
        xdot((j - 1) * dim + couplinggene) + JJ_global * ...
        (meanfield - x((j - 1) * dim + couplinggene));
end 

end


