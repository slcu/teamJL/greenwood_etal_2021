%%
% Model code from Greenwood et al., "a spatial model of the
% plant circadian clock reveals design principles for coordinated timing".
%
% by Isao Tokuda (Ritsumeikian University) and Mark Greenwood (University
% of Cambridge).
% 
% Entrain to LD then release to LL.
%%

clear all; close all

genenames = {'_CL', '_CLp', '_P97', '_P97p', '_P51', '_P51p', '_EL', '_ELp'};

%%%%%%% Set some simulation parameters 
periodnoise = 1; 

%%%%%%% Coupling regime
%%%%%%% nn = 4, 8 for 4 or 8 neighbour coupling, or nn = 0 for global
nn = 4; 

%%%%%%% Which gene to couple via. According to the order of 'genenames'.
couplinggenes = {1, 1, 1, 1, 1, 1};

%%%%%%% Coupling strengths for local, long-distance, and global coupling
%%%%%%% regimes
J_local_strengths = {0, 0.01, 0.10, 1.0, 2.0, 4.0};
    
J_long_strengths = {0, 0, 0, 0, 0, 0};

J_global_strengths = {0, 0, 0, 0, 0, 0};

%%%%%%% Simulate the PHYB null mutant?
%%%%%%% Set to 1 for WT light sensing, 0 for Lsens = 0 to a cells 
PHYB_level = 1; 

%%%%%%% Locations of the individual regions 
hypoc = [26, 30, 11, 36]; % x1, x2, y1, y2
roots = [27, 29, 37, 86]; 
roottip = [27, 29, 87, 95];

%%%%%%% Locations of the kymogram
for i = 1:28
  kymoline_x(i) = i;
  kymoline_y(i) = 12;
end
for i = 28 + 1:1:(91 + 4 - 12 + 28 + 1)
  kymoline_x(i) = 28;
  kymoline_y(i) = 12 + (i - 28);
end

%%%%%%% Locations of the 3X3 traces 
k = 1;
for i = -1:1 % x
  for j = -2:2 % y
    cotyl_x(k) = 14 + i;
    cotyl_y(k) = 12 + j;
    hypoc_x(k) = 28 + i;
    hypoc_y(k) = 25 + j;
    root_x(k) = 28 + i;
    root_y(k) = 62 + j;
    rtip_x(k) = 28 + i;
    rtip_y(k) = 89 + j;
    k = k + 1;
  end
end

load([pwd, '/program/ModelTemplate_full.mat'])

%%
for s = 1:length(J_local_strengths)
    
    rng(1)

    couplinggene = couplinggenes{s};

    for r = 1:2 % repeat simulations
 
        working_template = template;
                
        stages = 7;
        totalcell = (stages - 2) * 3 + sum(sum(working_template == 1));
        Timescaling = zeros(stages, totalcell);
        Lsens = zeros(stages, totalcell);
        neighbors = zeros(stages, totalcell, nn);
        cellnumber = zeros(stages, 1);
        celltype = zeros(stages, totalcell);

        for n = 1:stages
          
          if n > 2 % grow the working_template at the rt
            working_template(91 + n - 2, 27:29) = ones(1, 3); 
          end
          
          %%%%%%% Determine each cells position on the template, and assign 
          %%%%%%% light sensitivity and noise accordingly. 
          [locationx, locationy] = find(working_template' == 1);
          cell = length(locationy);
          cellnumber(n) = cell;

          for i = 1:cell
              
            if hypoc(1) <= locationx(i) && locationx(i) <= hypoc(2) &&...
                    hypoc(3) <= locationy(i) && locationy(i) <= hypoc(4)
                
              celltype(n, i) = 2; % hypocotyls   
              Lsens(n, i) = 1.00;
              Timescaling(n, i) =  1 + (0.72 / 25.66) * ...
                  (periodnoise * randn);
              
            elseif roots(1) <= locationx(i) && locationx(i) <= roots(2) &&...
                    roots(3) <= locationy(i) && locationy(i) <= roots(4)
                
              celltype(n, i) = 3;
              Lsens(n, i) = 0.65;
              Timescaling(n, i) = 1 + (2.05 / 28.26) * ...
                  (periodnoise * randn);
                
            elseif roottip(1) <= locationx(i) && locationx(i) <= roottip(2) &&...
                    roottip(3) <= locationy(i) && locationy(i) <= roottip(4)
                
              celltype(n, i) = 4;
              Lsens(n, i) = 0.95; 
              Timescaling(n, i) = 1 + (2.37 / 26.64) * ...
                  (periodnoise * randn);
              
            else
                
              celltype(n, i) = 1;  % cotyledons
              Lsens(n, i) = 1.60; 
              Timescaling(n, i)= 1 + (1.38 / 23.35) * ...
                  (periodnoise * randn); 
              
            end            
          end
         
          %%%%%%% Determine which cells are neighbors
          neighbor_offsets = {[-1, 0], [1, 0], [0, -1], [0, 1],...
              [-1, -1], [1, -1], [-1, 1], [1, 1]}; 
          for i = 1:cell
            for j = 1:cell
                for neighbor = 1:nn
                    condition = neighbor_offsets{neighbor};
                    if locationx(i) + condition(1) == locationx(j) && ...
                            locationy(i) + condition(2) == locationy(j)
                        neighbors(n, i, neighbor) = j;
                    end
                end
            end
          end
                                 
          %%%%%%% Handle changes in cell types caused by growth of the template 
          if n == 1
            for i = 1:cell
              celltype(n, i) = celltype(n, i);
              Lsens(n, i) = Lsens(n, i);
            end
          elseif n == 2
            for i = 1:cell
              celltype(n, i) = celltype(n - 1, i);
              Lsens(n, i) = Lsens(n - 1, i);
              Timescaling(n, i) = Timescaling(n - 1, i);
            end
          else
            for i = 1:previouscell
              if locationy(i) == (roottip(3) + (n - 3)) && ...
                26 < locationx(i) && locationx(i) < 30
                % root period is adopted to last layer of root-tip
                celltype(n, i) = 3;
                Lsens(n, i) = 0.65; 
                Timescaling(n, i) = 1 + (2.05 / 28.26) * ...
                    (periodnoise * randn);
              else
                celltype(n, i) = celltype(n - 1, i);
                Lsens(n, i) = Lsens(n - 1, i);
                Timescaling(n, i) = Timescaling(n - 1, i);
              end
            end
            
            celltype(n, cell - 15 + 1:cell) = ...
                celltype(n - 1, cell - 18 + 1:cell - 3);
            Lsens(n, cell - 15 + 1:cell) = ...
                Lsens(n - 1, cell - 18 + 1:cell - 3);
            Timescaling(n, cell - 15 + 1:cell) = ...
                Timescaling(n - 1, cell - 18 + 1:cell - 3);
            
          end

          previouscell = cell;
          
        end  
        
        %%%%%%% Simulate the LD cycle. 
        DT = 0.1;  % integration timestep 
        n_hours = 480;
        day_length = 12;
        cell = cellnumber(n);
        L = NaN(cell, round(n_hours / DT));

        for j = 1:cell

            count = 1;
            for time = 0:DT:n_hours

                if rem(time, 24) < day_length
                    L(j, count) = 1;
                else
                    L(j, count) = 0;
                end
                
                count = count + 1;
                
            end
        end

        %%%%%%%  Set coupling strengths
        JJ_local = J_local_strengths{s}; 
        JJ_long = J_long_strengths{s};
        JJ_global = J_global_strengths{s};
        dims = 12; % number of variables for individual cell model
        
        %%%%%%% Transient simulation
        n = 1;
        cell = cellnumber(n);
        
        Timescaling0 = NaN(cell, 1);
        Lsens0 = NaN(cell, 1);
        neighbors0 = NaN(cell, 1);
        celltype0 = NaN(cell, 1);
        x0 = zeros(cell * dims, 1);
 
        Timescaling0(1:cell, 1) = Timescaling(n, 1:cell);
        Lsens0(1:cell, 1) = Lsens(n, 1:cell);
        neighbors0(1:cell, 1:nn) = neighbors(n, 1:cell, 1:nn);
        celltype0(1:cell, 1) = celltype(n, 1:cell);

        trans = 4.0 * 24;
        opts = odeset('AbsTol', 1e-3);
        tspan = 0:DT:trans; % 0h to 96.0h
        [t, x] = ode45(@Coupled_GonzeModifiedODE, tspan, x0, opts,...
            Timescaling0, Lsens0, couplinggene, JJ_local, JJ_long, JJ_global,...
            nn, neighbors0, celltype0, L);
        
        %%%%%%% Light level is equal to all cells in the PHYB knock-out
        if PHYB_level == 0
            Lsens(:) = PHYB_level;
            L = zeros(size(L));
        elseif PHYB_level > 0 && PHYB_level < 1
            Lsens(:) = PHYB_level;
            L = ones(size(L));
        else
            L = ones(size(L));
        end

        %%%%%%% Main simulation with growing root tip
        x1 = NaN(1440, cellnumber(7) * dims); t1 = NaN(1440, 1);       
        count = 1;
        for n = 2:1:stages
          cell = cellnumber(n);
          Timescaling0(1:cell, 1) = Timescaling(n, 1:cell);
          Lsens0(1:cell, 1) = Lsens(n, 1:cell);
          neighbors0(1:cell, 1:nn) = neighbors(n, 1:cell, 1:nn);
          celltype0(1:cell, 1) = celltype(n, 1:cell);
    
          datalength = 1 * 24;
          if n > 2
            x0 = [x(end, 1:(cell - 18) * dims) ...
                    x(end, (cell - 21) * dims + 1:(cell - 18) * dims) ...
                    x(end, (cell - 18) * dims + 1:(cell - 15) * dims) ...
                    x(end, (cell - 15) * dims + 1:(cell - 12) * dims) ...
                    x(end, (cell - 12) * dims + 1:(cell - 9) * dims) ...
                    x(end, (cell - 9) * dims + 1:(cell - 6) * dims) ...
                    x(end, (cell - 6) * dims + 1:(cell - 3) * dims)];
          else
            x0 = x(end, :);
          end
          
          tspan = t(end) + DT:DT:t(end) + datalength;  
          [t, x] = ode45(@Coupled_GonzeModifiedODE, tspan, x0, opts,...
              Timescaling0, Lsens0, couplinggene, JJ_local, JJ_long,...
              JJ_global, nn, neighbors0, celltype0, L);
                 
          x1(count:count + 239, 1:size(x, 2)) = x;
          t1(count:count + 239, 1) = t; 
          count = count + 240;
          
        end
        
        %%%%%%% Process and save the data by gene
        for genename = 1:length(genenames) 

            %%%%%%% Identification of cell ID on 3x3 traces and collection of the time-traces
            cotylcell = NaN(1, size(cotyl_x, 2));
            hypoccell = NaN(1, size(hypoc_x, 2));
            rootcell = NaN(1, size(root_x, 2));
            for i = 1:size(cotyl_x, 2)
              for j = 1:1:cellnumber(stages)
                if locationx(j) == cotyl_x(i) && locationy(j) == cotyl_y(i)
                  cotylcell(i) = j;	
                elseif locationx(j) == hypoc_x(i) && locationy(j) == hypoc_y(i)
                  hypoccell(i) = j;	
                elseif locationx(j) == root_x(i) && locationy(j) == root_y(i)
                  rootcell(i) = j;	
                end
              end
            end
            
            trace_cotyl = NaN(size(cotyl_x, 2), size(x1, 1));
            trace_hypoc = NaN(size(hypoc_x, 2), size(x1, 1)); 
            trace_root = NaN(size(root_x, 2), size(x1, 1)); 
            for j = 1:1:size(cotyl_x, 2)
              trace_cotyl(j, :) = x1(:, (cotylcell(j) - 1) * dims + genename); 
              trace_hypoc(j, :) = x1(:, (hypoccell(j) - 1) * dims + genename); 
              trace_root(j, :) = x1(:, (rootcell(j) - 1) * dims + genename); 
            end

            %%%%%%% For 3x3 on moving root-tip
            rtipcell = NaN(1, size(rtip_x, 2));
            trace_rtip = NaN(size(rtip_x, 2), size(x1, 1)); 
            for n = 2:stages
              for i = 1:size(rtip_x, 2)
                for j = 1:cellnumber(n)
                  if locationx(j) == rtip_x(i) && locationy(j) == ...
                          (rtip_y(i) + (n - 2))
                    rtipcell(i) = j;	
                  end
                end
              end
                            
              for j = 1:size(rtip_x, 2)
                trace_rtip(j, (n - 2) * (datalength / DT) + 1:(n - 1) * ...
                    (datalength / DT)) = x1(( n - 2) * ...
                    (datalength / DT) + 1:( n - 1) * (datalength / DT),...
                    (rtipcell(j) - 1) * dims + genename); 
              end
            end

            %%%%%%% Convert data to 3d matrix
            all_idata = NaN(96, 55, size(t1, 1));
            for j = 1:cellnumber(7)
               all_idata(locationy(j), locationx(j), :) = ...
                   x1(:, ((j - 1) * dims + genename));
            end
               
            %%%%%%% Calculate kymographs with longitudinal sections
            XX = NaN(1, size(all_idata, 3) / 2 * size(kymoline_x, 2));
            YY = NaN(size(XX)); ZZ = NaN(size(XX));
            count = 1;
            for n = 1:2:size(all_idata, 3)
                for j = 1:size(kymoline_x, 2)            
                    if j <= 28
                        ZZ(1,  count) = nanmean(all_idata(kymoline_y(j) - 2:kymoline_y(j) + 2,...
                            kymoline_x(j), n));
                    else
                        ZZ(1,  count) = nanmean(all_idata(kymoline_y(j),...
                             kymoline_x(j) - 2:kymoline_x(j) + 2, n));
                    end
                    XX(1, count) = t1(n);
                    YY(1, count) = j;
                    count = count + 1;
                end
            end
                       
            %%%%%%%          
%             figure; scatter(XX, YY, 10, ZZ, 'filled')
%             set(gca, 'YDir', 'reverse', 'YLim', [1 112])
%             title(['WT under LD; ', genenames{genename}], 'Interpreter', 'none')
%             xlabel('Time (h)'); ylabel('Cell location')
            
            savepath = [pwd, '/saves/', num2str(nn), 'nn/',...
                'LL/period_noise', num2str(periodnoise),...
                '/coupling_gene', num2str(couplinggene),...
                '/PHYB', num2str(PHYB_level)];
            
            if ~exist(savepath, 'dir')
                mkdir(savepath)
            end
      
            save([savepath, '/LD_to_LL_Jlocal', num2str(JJ_local), '_Jlong',...
                num2str(JJ_long), '_Jglobal', num2str(JJ_global), '_sim',...
                num2str(r), genenames{genename}, '.mat'], 'trace_cotyl',...
                'trace_hypoc', 'trace_root', 'trace_rtip', 'all_idata');
                    
        end
        
    end
    
end
